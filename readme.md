# Spring Boot Starter

Start coding in a [ready-to-code development environment](https://www.gitpod.io/#https://gitlab.com/sg-vikalp/spring-boot/-/tree/develop):

Launch a web IDE [![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://www.gitpod.io/#https://gitlab.com/sg-vikalp/spring-boot/-/tree/develop)

Or you can run it from Maven directly using the Spring Boot Maven plugin. If you do this it will pick up changes that you make in the project immediately (changes to Java source files require a compile as well - most people use an IDE for this):

```
./mvnw spring-boot:run
```

# Objectives

The environment includes spring-boot-starter template from spring.io and tooling for build and run. It also has a mysql database. Refer to docker-compose.yml for the mysql database

- Create a table for todos with attributes (email, task, done, completed_on)
- Create api's for todos (get, put, post, delete) (only data based on email should be fetched/updated)
- Use a guard to restrict access based on valid email
- Include swagger with samples for testing functionality
